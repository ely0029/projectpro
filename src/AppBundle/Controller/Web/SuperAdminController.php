<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Employee;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

class SuperAdminController extends Controller {

  /**
    * @Route("/owner/login", name="superAdminLogin")
    */
   public function superAdminLoginAction(Request $request)
   {
     /** @var $session Session */
     $session = $request->getSession();

     $authErrorKey = Security::AUTHENTICATION_ERROR;
     $lastUsernameKey = Security::LAST_USERNAME;

     // get the error if any (works with forward and redirect -- see below)
     if ($request->attributes->has($authErrorKey)) {
         $error = $request->attributes->get($authErrorKey);
     } elseif (null !== $session && $session->has($authErrorKey)) {
         $error = $session->get($authErrorKey);
         $session->remove($authErrorKey);
     } else {
         $error = null;
     }

     if (!$error instanceof AuthenticationException) {
         $error = null; // The value does not come from the security component.
     }

     // last username entered by the user
     $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

     $csrfToken = $this->has('security.csrf.token_manager')
         ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
         : null;

       return $this->render('@FOSUser/Security/admin-login.html.twig', [
         'last_username' => $lastUsername,
         'error' => $error,
         'csrf_token' => $csrfToken,
       ]);
   }

   private function getEmployeeRecord($companyId) {
       if ($companyId) {
           $user = $this->getUser();
           $company = $this->getDoctrine()
               ->getRepository('AppBundle:Company')
               ->find($companyId);
           $employeeRecord = $this->getDoctrine()
               ->getRepository('AppBundle:Employee')
               ->findOneBy(['user' => $user, 'company' => $company]);

           return $employeeRecord;
       } else {
           return null;
       }
   }

    /**
     * @Route("/superadmin/{id}", name="showSuperAdminDashboard")
     */
    public function showSuperAdminDashboardAction($id, Request $request){

      $isQbIntegrated = $this->getDoctrine()
          ->getRepository('AppBundle:Company')
          ->find($id)
          ->isQbIntegrated();
      return $this->render(
                      'dashboard/super-admin-dashboard.html.twig', ['companyId' => $id, 'isQbIntegrated' => $isQbIntegrated,  'employeeRecord' => $this->getEmployeeRecord($id),
              'error' => $request->query->get('error')]
      );

    }

    /**
    * @Route("/superadmin/transactionlist/{id}", name="showSuperAdminTransactionList")
    */
   public function showSuperAdminTransactionListAction($id, Request $request) {
       $employeeRecord = $this->getEmployeeRecord($id);
       $employeeId = null;
       if ($employeeRecord) {
           $employeeId = $this->getEmployeeRecord($id)->getId();
       }

       return $this->render(
                       'dashboard/super-admin-transaction-list.html.twig', ['companyId' => $id,'employeeRecord' => $employeeRecord, 'employeeId' => $employeeId,
               'error' => $request->query->get('error')]
       );
   }

   /**
    * @Route("/superadmin/reports/{id}", name="showSuperAdminReports")
    */
   public function showSuperAdminReportsAction($id, Request $request) {
       $employeeRecord = $this->getEmployeeRecord($id);
       $employeeId = null;
       if ($employeeRecord) {
           $employeeId = $this->getEmployeeRecord($id)->getId();
       }

       return $this->render(
                       'dashboard/super-admin-reports.html.twig', ['companyId' => $id,'employeeRecord' => $employeeRecord, 'employeeId' => $employeeId,
               'error' => $request->query->get('error')]
       );
   }

   /**
    * @Route("/superadmin/user/{id}", name="showSuperAdminUser")
    */
   public function showSuperAdminUserAction($id, Request $request) {
       $employeeRecord = $this->getEmployeeRecord($id);
       $employeeId = null;
       if ($employeeRecord) {
           $employeeId = $this->getEmployeeRecord($id)->getId();
       }

       return $this->render(
                       'dashboard/super-admin-user.html.twig', ['companyId' => $id,'employeeRecord' => $employeeRecord, 'employeeId' => $employeeId,
               'error' => $request->query->get('error')]
       );
   }

   /**
    *
    * @Route("/admin/new/user", name="superAdminNewUser")
    *
    */
   public function addUserAction(Request $request)
   {
       $firstName = $request->request->get('fname');
       $lastName = $request->request->get('lname');
       $email =   $request->request->get('email');
       $password = 'P@ssw0rd$$';
       $accountantEmail = '';
       $em = $this->getDoctrine()->getManager();

       $userManager = $this->get('fos_user.user_manager');
       $error = $request->query->get('error');

       $user = $em
       ->getRepository('AppBundle:User')
       ->create($userManager, $firstName, $lastName, $email, $password, $accountantEmail);

       return new JsonResponse(array('error' => $error));
   }

}
